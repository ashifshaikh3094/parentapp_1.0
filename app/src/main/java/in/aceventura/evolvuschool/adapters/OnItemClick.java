package in.aceventura.evolvuschool.adapters;

import java.util.ArrayList;

public interface OnItemClick {
    void onClick (boolean b);
}
