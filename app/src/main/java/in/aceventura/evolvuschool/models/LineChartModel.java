package in.aceventura.evolvuschool.models;

import java.util.ArrayList;

public class LineChartModel {
   String mSubject;
    String mPercentage;

    public String getmSubject() {
        return mSubject;
    }

    public void setmSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    public String getmPercentage() {
        return mPercentage;
    }

    public void setmPercentage(String mPercentage) {
        this.mPercentage = mPercentage;
    }
}
